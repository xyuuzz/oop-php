<?php

    // include "class.php";
    // include "frog.php";
    // include "Ape.php";
    require ("class.php");
    require ("Ape.php");
    require ("frog.php");

    // spl_autoload_register(function ($class_name){
    //     include $class_name " .php";
    // })
    function enter(){
        echo "<br>";
    }

    echo "<h1> OOP PHP </h1>";

    $sheep = new Animal("shaun");
    $sheep -> legs(2);
    $sheep -> cold_blooded("false");
    echo $sheep->name; // "shaun"
    enter();
    echo "Shaun mempunyai kaki " . $sheep->legs; // 2
    enter();
    echo "Apakah shaun berdarah dingin? " . $sheep->cold_blooded; // false

    enter();
    enter();

    $sungokong = new Ape("kera sakti");
    echo $sungokong ->name;
    enter();
    echo $sungokong->yell(); // "Auooo"

    enter();
    enter();

    $kodok = new Frog("buduk");
    $kodok -> legs(4);
    echo $kodok ->name; 
    enter();
    echo "katak merupakan hewan berkaki " . $kodok ->legs;
    enter();
    echo $kodok->jump() ; // "hop hop"

    enter();
    enter();

    var_dump($sheep);
    enter();
    var_dump($sungokong);
    enter();
    var_dump($kodok);
    enter();


?>